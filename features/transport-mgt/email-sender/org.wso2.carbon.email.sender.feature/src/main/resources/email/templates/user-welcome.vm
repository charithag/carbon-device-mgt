#*
    Copyright (c) 2020, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.

    Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
    Version 2.0 (the "License"); you may not use this file except
    in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied. See the License for the
    specific language governing permissions and limitations
    under the License.
*#
<EmailConfig>
    <Subject>Welcome to Entgra Evaluation Cloud</Subject>
    <Body>
    <![CDATA[
    <html>
    <head>
        <title>Entgra IoT Server</title>
    </head>
    <body style="color: #666666; background-color:#cdcdcd; padding: 0px; margin: 0px;">
    <div style="background-color:#cdcdcd; font-length: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; padding: 20px 0px; margin: 0px;">
        <div style="width: 86%; max-width: 650px; padding: 2%; background-color: #ffffff; margin: auto; border-radius: 14px;">
            <div style="background-color: #ffebcc; line-height: 0px; border-top-left-radius: 10px; border-top-right-radius: 10px; padding: 10px;">
                <div style="display: inline-block; line-height: 0px;">
                    <img alt="entgra" src="https://storage.googleapis.com/cdn-entgra/logo.png" height="50px" width="143px" />
                </div>
            </div>
            <div style="background-color: #ffffff; line-height: 170%; color: #666666; padding: 20px 25px;">
                <p style="font-length: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 5px 0px 20px;">
                    Hi $first-name,
                </p>
                <div>
                    <p style="font-length: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 20px 0px 5px;">
                        Welcome to Entgra Evaluation Cloud!! Entgra server support offers managing Android, iOS and Windows
                        devices along with a wide range of features that support cooperate (COPE) or personal device (BYOD)
                        enrollments.
                    </p>
                </div>
                <div>
                    <h3>Access different portals</h3>
                    <p style="font-size: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 5px 0px;">Your log-in credentials to any of our portals(endpoint-mgt, store, publisher) are,</p>
                    <p style="font-size: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 5px 0px;"><b>Username: </b> $portal-username</p>
                    <p style="font-size: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 5px 0px;"><b>Password: </b> password provided at registration for an evaluation account.</p>
                    <h4>Endpoint management portal URL: <a href="$base-url-https/endpoint-mgt/">$base-url-https/endpoint-mgt/</a></h4>
                    <p style="font-length: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 20px 0px 5px;">
                        This is the portal used to send operations and policies to devices and overall management of
                        the server.
                    </p>
                    <h4>Application store portal URL: <a href="$base-url-https/store">$base-url-https/store</a></h4>
                    <p style="font-length: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 20px 0px 5px;">
                        This is an in-house corporate app store where you can host all your corporate applications.
                        Users may browse apps and install them to their devices if the administrator has made the apps
                        publicly visible to users. Administrator can install, uninstall and update apps in the user
                        device or device groups.
                    </p>
                    <h4>Application publishing portal URL: <a href="$base-url-https/publisher">$base-url-https/publisher</a></h4>
                    <p style="font-length: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 20px 0px 5px;">
                        The portal for publishing new applications for internal use. This is the developer view of the
                        enterprise application store that comes with the product.
                    </p>
                </div>
                <div>
                    <h3>Enroll a device</h3>
                    <p style="font-length: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 20px 0px 5px;">
                        Please find here a set of videos on how to enroll and onboard devices to our Cloud platform.
                        <a href="https://entgra-documentation.gitlab.io/v4.0.0/docs/product-guide/enrollment-guide/enroll-cloud/">[Cloud Enrollment Guide]</a>
                        When enrolling a device, Make sure to use the following log-in format:
                    </p>
                    <p style="font-size: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 5px 0px;"><b>Organisation:</b> $tenant-domain</p>
                    <p style="font-size: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 5px 0px;"><b>Username:</b> $agent-username</p>
                    <p style="font-size: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 5px 0px;"><b>Password:</b> password provided at registration.</p>
                </div>
                <div>
                    <p style="font-length: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 20px 0px 5px;">
                        If you have any further questions, please reach out to us using your registered mail to
                        <a href="bizdev-group@entgra.io"> bizdev-group@entgra.io.</a> Looking forward to working with you.
                    </p>
                    <p style="font-length: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 20px 0px 5px;">
                        Best Regards,
                    </p>
                    <p style="font-size: 1em; font-family: Arial, Helvetica; line-height: 170%; color: #666666; margin: 5px 0px;">
                        Entgra Cloud Team
                    </p>
                </div>
            </div>
        </div>
    </div>
    </body>
    </html>
    ]]>
    </Body>
</EmailConfig>
